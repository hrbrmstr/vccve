package main

import (
	"bytes"
	"encoding/json"
	"os/exec"
	"testing"

	"codeberg.org/hrbrmstr/vccve/pkg"
)

func TestMain(t *testing.T) {

	t.Run("NVD & KEV", func(t *testing.T) {
		cmd := exec.Command("go", "run", "main.go", "CVE-2024-23334")
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}

		var result pkg.Vccve
		err = json.Unmarshal(out.Bytes(), &result)
		if err != nil {
			t.Fatalf("failed to unmarshal JSON: %v", err)
		}

		if *result.Vckev.Data[0].VendorProject != "aiohttp" {
			t.Errorf("unexpected output:\nexpected: %s\nactual: %s", "aiohttp", *result.Vckev.Data[0].VendorProject)
		}
		if *result.Nvdv2.Data[0].SourceIdentifier != "security-advisories@github.com" {
			t.Errorf("unexpected output:\nexpected: %s\nactual: %s", "security-advisories@github.com", *result.Nvdv2.Data[0].SourceIdentifier)
		}
	})

}
