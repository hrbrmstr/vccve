package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"codeberg.org/hrbrmstr/vccve/pkg"
)

type VulnData struct {
	NVDV2 json.RawMessage `json:"nvdv2"`
	VCKEV json.RawMessage `json:"vckev"`
}

func main() {
	apiKey := os.Getenv("VULNCHECK_API_KEY")
	if apiKey == "" {
		log.Fatal("Please set the VULNCHECK_API_KEY environment variable.")
	}

	if len(os.Args) < 2 {
		log.Fatal("Please provide the CVE as a command-line argument.")
	}
	cve := os.Args[1]

	nvdv2Data, err := pkg.FetchCVEData(apiKey, fmt.Sprintf("https://api.vulncheck.com/v3/index/nist-nvd2?cve=%s", cve))
	if err != nil {
		log.Fatalf("Error fetching NVDV2 data: %v", err)
	}

	vckevData, err := pkg.FetchCVEData(apiKey, fmt.Sprintf("https://api.vulncheck.com/v3/index/vulncheck-kev?cve=%s", cve))
	if err != nil {
		log.Fatalf("Error fetching VCKEV data: %v", err)
	}

	vulnData := VulnData{
		NVDV2: nvdv2Data,
		VCKEV: vckevData,
	}

	jsonData, err := json.MarshalIndent(vulnData, "", "  ")
	if err != nil {
		log.Fatalf("Error marshaling JSON: %v", err)
	}

	os.Stdout.Write(jsonData)
}
