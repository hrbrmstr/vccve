---
format: gfm
engine: knitr
---

# vccve: Retrieve Combined VulnCheck Extended KEV & NIST NVDv2 CVE Information

VulnCheck has some new, free [API endpoints](https://docs.vulncheck.com/api) for the cybersecurity community.

Two extremely useful ones are for their extended version of CISA's KEV, and an in-situ replacement for NVD's sad excuse for an API and soon-to-be-removed JSON feeds.

This Go CLI package makes it easy to retrieve combined CVE data from both sources. Just feed it a CVE ID and you'll get a JSON blob with the KEV and NVD data to `stdout`.

You'll need a [free API key](https://vulncheck.com/token/newtoken) from VulnCheck stored in `VULNCHECK_API_KEY` to use it.

## Building

Use the [Justfile](https://just.systems/):

```bash
just build
```

## Installation

```bash
go install codeberg.org/hrbrmstr/vccve/cmd/vccve@latest
```

or grab binaries for your platform from [the releases](https://codeberg.org/hrbrmstr/vccve/releases).


## Usage

```{bash run, cache=TRUE}
vccve CVE-2024-23334
```