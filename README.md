

# vccve: Retrieve Combined VulnCheck Extended KEV & NIST NVDv2 CVE Information

VulnCheck has some new, free [API
endpoints](https://docs.vulncheck.com/api) for the cybersecurity
community.

Two extremely useful ones are for their extended version of CISA’s KEV,
and an in-situ replacement for NVD’s sad excuse for an API and
soon-to-be-removed JSON feeds.

This Go CLI package makes it easy to retrieve combined CVE data from
both sources. Just feed it a CVE ID and you’ll get a JSON blob with the
KEV and NVD data to `stdout`.

You’ll need a [free API key](https://vulncheck.com/token/newtoken) from
VulnCheck stored in `VULNCHECK_API_KEY` to use it.

## Building

Use the [Justfile](https://just.systems/):

``` bash
just build
```

## Installation

``` bash
go install codeberg.org/hrbrmstr/vccve/cmd/vccve@latest
```

or grab binaries for your platform from [the
releases](https://codeberg.org/hrbrmstr/vccve/releases).

## Usage

``` bash
vccve CVE-2024-23334
```

    {
      "nvdv2": {
        "_benchmark": 0.031793,
        "_meta": {
          "timestamp": "2024-03-23T10:23:08.664629793Z",
          "index": "nist-nvd2",
          "limit": 100,
          "total_documents": 1,
          "sort": "_id",
          "parameters": [
            {
              "name": "cve",
              "format": "CVE-YYYY-N{4-7}"
            },
            {
              "name": "alias"
            },
            {
              "name": "iava",
              "format": "[0-9]{4}[A-Z-0-9]+"
            },
            {
              "name": "threat_actor"
            },
            {
              "name": "mitre_id"
            },
            {
              "name": "misp_id"
            },
            {
              "name": "ransomware"
            },
            {
              "name": "botnet"
            },
            {
              "name": "published"
            },
            {
              "name": "lastModStartDate",
              "format": "YYYY-MM-DD"
            },
            {
              "name": "lastModEndDate",
              "format": "YYYY-MM-DD"
            }
          ],
          "order": "desc",
          "page": 1,
          "total_pages": 1,
          "max_pages": 6,
          "first_item": 1,
          "last_item": 1
        },
        "data": [
          {
            "id": "CVE-2024-23334",
            "sourceIdentifier": "security-advisories@github.com",
            "vulnStatus": "Modified",
            "published": "2024-01-29T23:15:08.563",
            "lastModified": "2024-02-09T03:15:09.603",
            "descriptions": [
              {
                "lang": "en",
                "value": "aiohttp is an asynchronous HTTP client/server framework for asyncio and Python. When using aiohttp as a web server and configuring static routes, it is necessary to specify the root path for static files. Additionally, the option 'follow_symlinks' can be used to determine whether to follow symbolic links outside the static root directory. When 'follow_symlinks' is set to True, there is no validation to check if reading a file is within the root directory. This can lead to directory traversal vulnerabilities, resulting in unauthorized access to arbitrary files on the system, even when symlinks are not present.  Disabling follow_symlinks and using a reverse proxy are encouraged mitigations.  Version 3.9.2 fixes this issue."
              },
              {
                "lang": "es",
                "value": "aiohttp es un framework cliente/servidor HTTP asíncrono para asyncio y Python. Cuando se utiliza aiohttp como servidor web y se configuran rutas estáticas, es necesario especificar la ruta raíz para los archivos estáticos. Además, la opción 'follow_symlinks' se puede utilizar para determinar si se deben seguir enlaces simbólicos fuera del directorio raíz estático. Cuando 'follow_symlinks' se establece en Verdadero, no hay validación para verificar si la lectura de un archivo está dentro del directorio raíz. Esto puede generar vulnerabilidades de directory traversal, lo que resulta en acceso no autorizado a archivos arbitrarios en el sistema, incluso cuando no hay enlaces simbólicos presentes. Se recomiendan como mitigaciones deshabilitar follow_symlinks y usar un proxy inverso. La versión 3.9.2 soluciona este problema."
              }
            ],
            "references": [
              {
                "url": "https://github.com/aio-libs/aiohttp/commit/1c335944d6a8b1298baf179b7c0b3069f10c514b",
                "source": "security-advisories@github.com",
                "tags": [
                  "Patch"
                ]
              },
              {
                "url": "https://github.com/aio-libs/aiohttp/pull/8079",
                "source": "security-advisories@github.com",
                "tags": [
                  "Patch"
                ]
              },
              {
                "url": "https://github.com/aio-libs/aiohttp/security/advisories/GHSA-5h86-8mv2-jq9f",
                "source": "security-advisories@github.com",
                "tags": [
                  "Exploit",
                  "Mitigation",
                  "Vendor Advisory"
                ]
              },
              {
                "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/ICUOCFGTB25WUT336BZ4UNYLSZOUVKBD/",
                "source": "security-advisories@github.com"
              },
              {
                "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/XXWVZIVAYWEBHNRIILZVB3R3SDQNNAA7/",
                "source": "security-advisories@github.com",
                "tags": [
                  "Mailing List"
                ]
              }
            ],
            "metrics": {
              "cvssMetricV31": [
                {
                  "source": "nvd@nist.gov",
                  "type": "Primary",
                  "cvssData": {
                    "version": "3.1",
                    "vectorString": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N",
                    "attackVector": "NETWORK",
                    "attackComplexity": "LOW",
                    "privilegesRequired": "NONE",
                    "userInteraction": "NONE",
                    "scope": "UNCHANGED",
                    "confidentialityImpact": "HIGH",
                    "integrityImpact": "NONE",
                    "availabilityImpact": "NONE",
                    "baseScore": 7.5,
                    "baseSeverity": "HIGH"
                  },
                  "exploitabilityScore": 3.9,
                  "impactScore": 3.6
                },
                {
                  "source": "security-advisories@github.com",
                  "type": "Secondary",
                  "cvssData": {
                    "version": "3.1",
                    "vectorString": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:N/A:N",
                    "attackVector": "NETWORK",
                    "attackComplexity": "HIGH",
                    "privilegesRequired": "NONE",
                    "userInteraction": "NONE",
                    "scope": "UNCHANGED",
                    "confidentialityImpact": "HIGH",
                    "integrityImpact": "NONE",
                    "availabilityImpact": "NONE",
                    "baseScore": 5.9,
                    "baseSeverity": "MEDIUM"
                  },
                  "exploitabilityScore": 2.2,
                  "impactScore": 3.6
                }
              ]
            },
            "weaknesses": [
              {
                "source": "security-advisories@github.com",
                "type": "Primary",
                "description": [
                  {
                    "lang": "en",
                    "value": "CWE-22"
                  }
                ]
              }
            ],
            "configurations": [
              {
                "nodes": [
                  {
                    "operator": "OR",
                    "cpeMatch": [
                      {
                        "vulnerable": true,
                        "criteria": "cpe:2.3:a:aiohttp:aiohttp:*:*:*:*:*:*:*:*",
                        "versionStartIncluding": "1.0.5",
                        "versionEndExcluding": "3.9.2",
                        "matchCriteriaId": "CC18B2A9-9D80-4A6E-94E7-8FC010D8FC70"
                      }
                    ]
                  }
                ]
              },
              {
                "nodes": [
                  {
                    "operator": "OR",
                    "cpeMatch": [
                      {
                        "vulnerable": true,
                        "criteria": "cpe:2.3:o:fedoraproject:fedora:39:*:*:*:*:*:*:*",
                        "matchCriteriaId": "B8EDB836-4E6A-4B71-B9B2-AA3E03E0F646"
                      }
                    ]
                  }
                ]
              }
            ],
            "_timestamp": "2024-02-09T05:33:33.170054Z"
          }
        ]
      },
      "vckev": {
        "_benchmark": 0.069464,
        "_meta": {
          "timestamp": "2024-03-23T10:23:08.798358336Z",
          "index": "vulncheck-kev",
          "limit": 100,
          "total_documents": 1,
          "sort": "_id",
          "parameters": [
            {
              "name": "cve",
              "format": "CVE-YYYY-N{4-7}"
            },
            {
              "name": "alias"
            },
            {
              "name": "iava",
              "format": "[0-9]{4}[A-Z-0-9]+"
            },
            {
              "name": "threat_actor"
            },
            {
              "name": "mitre_id"
            },
            {
              "name": "misp_id"
            },
            {
              "name": "ransomware"
            },
            {
              "name": "botnet"
            },
            {
              "name": "published"
            },
            {
              "name": "lastModStartDate",
              "format": "YYYY-MM-DD"
            },
            {
              "name": "lastModEndDate",
              "format": "YYYY-MM-DD"
            },
            {
              "name": "pubStartDate",
              "format": "YYYY-MM-DD"
            },
            {
              "name": "pubEndDate",
              "format": "YYYY-MM-DD"
            }
          ],
          "order": "desc",
          "page": 1,
          "total_pages": 1,
          "max_pages": 6,
          "first_item": 1,
          "last_item": 1
        },
        "data": [
          {
            "vendorProject": "aiohttp",
            "product": "aiohttp",
            "shortDescription": "aiohttp is an asynchronous HTTP client/server framework for asyncio and Python. When using aiohttp as a web server and configuring static routes, it is necessary to specify the root path for static files. Additionally, the option 'follow_symlinks' can be used to determine whether to follow symbolic links outside the static root directory. When 'follow_symlinks' is set to True, there is no validation to check if reading a file is within the root directory. This can lead to directory traversal vulnerabilities, resulting in unauthorized access to arbitrary files on the system, even when symlinks are not present.  Disabling follow_symlinks and using a reverse proxy are encouraged mitigations.  Version 3.9.2 fixes this issue.",
            "vulnerabilityName": "aiohttp aiohttp Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal')",
            "required_action": "Apply remediations or mitigations per vendor instructions or discontinue use of the product if remediation or mitigations are unavailable.",
            "knownRansomwareCampaignUse": "Known",
            "cve": [
              "CVE-2024-23334"
            ],
            "vulncheck_xdb": [
              {
                "xdb_id": "231b48941355",
                "xdb_url": "https://vulncheck.com/xdb/231b48941355",
                "date_added": "2024-02-28T22:30:21Z",
                "exploit_type": "infoleak",
                "clone_ssh_url": "git@github.com:ox1111/CVE-2024-23334.git"
              },
              {
                "xdb_id": "f1d001911304",
                "xdb_url": "https://vulncheck.com/xdb/f1d001911304",
                "date_added": "2024-03-19T16:28:56Z",
                "exploit_type": "infoleak",
                "clone_ssh_url": "git@github.com:jhonnybonny/CVE-2024-23334.git"
              }
            ],
            "vulncheck_reported_exploitation": [
              {
                "url": "https://cyble.com/blog/cgsi-probes-shadowsyndicate-groups-possible-exploitation-of-aiohttp-vulnerability-cve-2024-23334/",
                "date_added": "2024-03-15T00:00:00Z"
              }
            ],
            "date_added": "2024-03-15T00:00:00Z",
            "_timestamp": "2024-03-23T09:30:18.701676Z"
          }
        ]
      }
    }
