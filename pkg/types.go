package pkg

type Vccve struct {
	Nvdv2 *Nvdv2 `json:"nvdv2,omitempty"`
	Vckev *Vckev `json:"vckev,omitempty"`
}

type Nvdv2 struct {
	Benchmark *float64     `json:"_benchmark,omitempty"`
	Meta      *Meta        `json:"_meta,omitempty"`
	Data      []Nvdv2Datum `json:"data,omitempty"`
}

type Nvdv2Datum struct {
	ID               *string         `json:"id,omitempty"`
	SourceIdentifier *string         `json:"sourceIdentifier,omitempty"`
	VulnStatus       *string         `json:"vulnStatus,omitempty"`
	Published        *string         `json:"published,omitempty"`
	LastModified     *string         `json:"lastModified,omitempty"`
	Descriptions     []Description   `json:"descriptions,omitempty"`
	References       []Reference     `json:"references,omitempty"`
	Metrics          *Metrics        `json:"metrics,omitempty"`
	Weaknesses       []Weakness      `json:"weaknesses,omitempty"`
	Configurations   []Configuration `json:"configurations,omitempty"`
	Timestamp        *string         `json:"_timestamp,omitempty"`
}

type Configuration struct {
	Nodes []Node `json:"nodes,omitempty"`
}

type Node struct {
	Operator *string    `json:"operator,omitempty"`
	CpeMatch []CpeMatch `json:"cpeMatch,omitempty"`
}

type CpeMatch struct {
	Vulnerable            *bool   `json:"vulnerable,omitempty"`
	Criteria              *string `json:"criteria,omitempty"`
	VersionStartIncluding *string `json:"versionStartIncluding,omitempty"`
	VersionEndExcluding   *string `json:"versionEndExcluding,omitempty"`
	MatchCriteriaID       *string `json:"matchCriteriaId,omitempty"`
}

type Description struct {
	Lang  *string `json:"lang,omitempty"`
	Value *string `json:"value,omitempty"`
}

type Metrics struct {
	CvssMetricV31 []CvssMetricV31 `json:"cvssMetricV31,omitempty"`
}

type CvssMetricV31 struct {
	Source              *string   `json:"source,omitempty"`
	Type                *string   `json:"type,omitempty"`
	CvssData            *CvssData `json:"cvssData,omitempty"`
	ExploitabilityScore *float64  `json:"exploitabilityScore,omitempty"`
	ImpactScore         *float64  `json:"impactScore,omitempty"`
}

type CvssData struct {
	Version               *string  `json:"version,omitempty"`
	VectorString          *string  `json:"vectorString,omitempty"`
	AttackVector          *string  `json:"attackVector,omitempty"`
	AttackComplexity      *string  `json:"attackComplexity,omitempty"`
	PrivilegesRequired    *string  `json:"privilegesRequired,omitempty"`
	UserInteraction       *string  `json:"userInteraction,omitempty"`
	Scope                 *string  `json:"scope,omitempty"`
	ConfidentialityImpact *string  `json:"confidentialityImpact,omitempty"`
	IntegrityImpact       *string  `json:"integrityImpact,omitempty"`
	AvailabilityImpact    *string  `json:"availabilityImpact,omitempty"`
	BaseScore             *float64 `json:"baseScore,omitempty"`
	BaseSeverity          *string  `json:"baseSeverity,omitempty"`
}

type Reference struct {
	URL    *string  `json:"url,omitempty"`
	Source *string  `json:"source,omitempty"`
	Tags   []string `json:"tags,omitempty"`
}

type Weakness struct {
	Source      *string       `json:"source,omitempty"`
	Type        *string       `json:"type,omitempty"`
	Description []Description `json:"description,omitempty"`
}

type Meta struct {
	Timestamp      *string     `json:"timestamp,omitempty"`
	Index          *string     `json:"index,omitempty"`
	Limit          *int64      `json:"limit,omitempty"`
	TotalDocuments *int64      `json:"total_documents,omitempty"`
	Sort           *string     `json:"sort,omitempty"`
	Parameters     []Parameter `json:"parameters,omitempty"`
	Order          *string     `json:"order,omitempty"`
	Page           *int64      `json:"page,omitempty"`
	TotalPages     *int64      `json:"total_pages,omitempty"`
	MaxPages       *int64      `json:"max_pages,omitempty"`
	FirstItem      *int64      `json:"first_item,omitempty"`
	LastItem       *int64      `json:"last_item,omitempty"`
}

type Parameter struct {
	Name   *string `json:"name,omitempty"`
	Format *Format `json:"format,omitempty"`
}

type Vckev struct {
	Benchmark *float64     `json:"_benchmark,omitempty"`
	Meta      *Meta        `json:"_meta,omitempty"`
	Data      []VckevDatum `json:"data,omitempty"`
}

type VckevDatum struct {
	VendorProject                 *string                         `json:"vendorProject,omitempty"`
	Product                       *string                         `json:"product,omitempty"`
	ShortDescription              *string                         `json:"shortDescription,omitempty"`
	VulnerabilityName             *string                         `json:"vulnerabilityName,omitempty"`
	RequiredAction                *string                         `json:"required_action,omitempty"`
	KnownRansomwareCampaignUse    *string                         `json:"knownRansomwareCampaignUse,omitempty"`
	Cve                           []string                        `json:"cve,omitempty"`
	VulncheckXdb                  []VulncheckXdb                  `json:"vulncheck_xdb,omitempty"`
	VulncheckReportedExploitation []VulncheckReportedExploitation `json:"vulncheck_reported_exploitation,omitempty"`
	DateAdded                     *string                         `json:"date_added,omitempty"`
	Timestamp                     *string                         `json:"_timestamp,omitempty"`
}

type VulncheckReportedExploitation struct {
	URL       *string `json:"url,omitempty"`
	DateAdded *string `json:"date_added,omitempty"`
}

type VulncheckXdb struct {
	XdbID       *string `json:"xdb_id,omitempty"`
	XdbURL      *string `json:"xdb_url,omitempty"`
	DateAdded   *string `json:"date_added,omitempty"`
	ExploitType *string `json:"exploit_type,omitempty"`
	CloneSSHURL *string `json:"clone_ssh_url,omitempty"`
}

type Format string

const (
	CveYyyyN47 Format = "CVE-YYYY-N{4-7}"
	The094AZ09 Format = "[0-9]{4}[A-Z-0-9]+"
	YyyyMmDD   Format = "YYYY-MM-DD"
)
