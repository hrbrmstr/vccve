# List all tasks
@default:
  just --list

# Init the project
@init:
  go mod init

# Freshen up go.mod
@tidy:
  go mod tidy

# Build for macOS
@build-macos:
  GOOS=darwin GOARCH=arm64 go build -o bin/aarch64-vccve cmd/vccve/main.go
  GOOS=darwin GOARCH=amd64 go build -o bin/x86_64-vccve cmd/vccve/main.go
  lipo -create -output bin/vccve-macos bin/aarch64-vccve bin/x86_64-vccve
  rm -f bin/aarch64-vccve bin/x86_64-vccve

# Build for Linux
build-linux:
  GOOS=linux GOARCH=amd64 go build -o bin/vccve-linux cmd/vccve/main.go

# Build for Windows
build-windows:
  GOOS=windows GOARCH=amd64 go build -o bin/vccve-windows.exe cmd/vccve/main.go

# Build for all platforms
@build: build-macos build-linux build-windows

# Test the functionality
@test:
  go test ./...